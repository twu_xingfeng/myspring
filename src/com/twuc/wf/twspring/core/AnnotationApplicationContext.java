package com.twuc.wf.twspring.core;

import com.twuc.wf.twspring.annotations.*;
import com.twuc.wf.twspring.entity.RouteEntity;
import com.twuc.wf.twspring.exceptions.MultipleInjectConstructorException;
import com.twuc.wf.twspring.exceptions.NoSuchBeanDefinitionException;
import com.twuc.wf.twspring.factory.BeanFactory;
import com.twuc.wf.twspring.simplehttpserver.contract.RequestMethod;
import com.twuc.wf.twspring.utils.ClassUtil;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.twuc.wf.twspring.constant.CONSTANT.pInfo;

public class AnnotationApplicationContext {

    private Class<?> context;

    public static Map<String, RouteEntity> getMappingMap = new HashMap<>();
    public static Map<String, RouteEntity>  postMappingMap = new HashMap<>();
    public static Map<Class<?>, Class<?>>  exceptionHandlerMap = new HashMap<>();

    AnnotationApplicationContext(Class<?> context) {
        this.context = context;
        this.scanAnnotations();
    }

    private void scanAnnotations() {


        List<Class<?>> classes = ClassUtil.getAllClassByPackageName(this.context.getPackage());

        for(Class<?> c:classes){

            // scan @Component
            Component component = c.getDeclaredAnnotation(Component.class);
            if(null != component) {
                BeanFactory.beansSet.add(c);
            }

            // scan @RestController
            RestController restController = c.getDeclaredAnnotation(RestController.class);
            if(null != restController){
                BeanFactory.beansSet.add(c);

                Method[] methods = c.getMethods();
                for(Method m:methods){
                    RequestMapping requestMapping = m.getDeclaredAnnotation(RequestMapping.class);
                    if(null != requestMapping){
                        if (requestMapping.method() == RequestMethod.POST) {
                            postMappingMap.put(restController.value() + requestMapping.value(), new RouteEntity(c, m));
                        } else {
                            getMappingMap.put((restController.value() + requestMapping.value()).split("\\?")[0], new RouteEntity(c, m));
                        }
                    }
                }
            }

            // scan @ControllerAdvice
            ControllerAdvice controllerAdvice = c.getDeclaredAnnotation(ControllerAdvice.class);
            if(null != controllerAdvice){
                BeanFactory.beansSet.add(c);

                for(Class<?> packageClz:controllerAdvice.basePackagesClasses()) {
                    exceptionHandlerMap.put(packageClz, c);
                }
            }

            // scan @Configuration and @Bean to configurationBeanMap
            Configuration configuration = c.getDeclaredAnnotation(Configuration.class);
            if(null != configuration) {
                for (Method method : c.getMethods()){
                    Bean bean = method.getDeclaredAnnotation(Bean.class);
                    if(null != bean){
                        Class<?> configBeanType = method.getReturnType();
                        BeanFactory.configurationBeanMap.put(configBeanType,new RouteEntity(c,method));
                    }
                }
            }

        }

        pInfo("[ "+Thread.currentThread().getName()+" ] " + BeanFactory.configurationBeanMap.toString());
        pInfo("[ "+Thread.currentThread().getName()+" ] " + getMappingMap.toString());
        pInfo("[ "+Thread.currentThread().getName()+" ] " + postMappingMap.toString());
        pInfo("[ "+Thread.currentThread().getName()+" ] " + exceptionHandlerMap.toString());
    }


    public Object getBean(Class<?> clz) throws IllegalAccessException, NoSuchBeanDefinitionException, InstantiationException, MultipleInjectConstructorException, InvocationTargetException {
        return BeanFactory.getBean(clz);
    }
}
